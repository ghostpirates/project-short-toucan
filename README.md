# project-short-toucan

[![Build Status](https://img.shields.io/badge/rhythym-feelin'%20it-brightgreen)](https://gitlab.com/ghostpirates)
[![Build Status](https://img.shields.io/badge/ride-feelin'%20it-brightgreen)](https://gitlab.com/ghostpirates)
[![Build Status](https://img.shields.io/badge/jamaica-coming%20along-yellow)](https://gitlab.com/ghostpirates)
[![Build Status](https://img.shields.io/badge/bobsled%20time-undetermined-red)](https://gitlab.com/ghostpirates)

It's a wavy line thing!

[demo link](https://utilities.lcprojects.com.au/project-short-toucan)
